#!/usr/bin/env python
#coding:utf-8

import sys
import subprocess
import tempfile
import urllib

text = sys.stdin.read()

chart_url_template = ('http://chart.apis.google.com/chart?'
                      'cht=qr&chs=400x400&chl={data}&chld=H|O')

chart_url = chart_url_template.format(data=urllib.quote(text))

with tempfile.NamedTemporaryFile(mode='w', suffix='.png') as f:
    subprocess.check_call(['curl', '-L', chart_url],
                         stdout=f, stderr=sys.stderr)
    subprocess.check_call(['shotwell', f.name])

if __name__ == "__main__":
    import sys
    if sys.getdefaultencoding() == 'ascii':
        reload(sys)
        sys.setdefaultencoding('utf-8')





